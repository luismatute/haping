'use strict';

// Dependencies ==========================
	var angular = require('angular');

// Define the list of controllers here
	// require('./ctrl1');
	// require('./ctrl2');

// Exposing Module ============================
	module.exports = angular.module('app.controllers', []);